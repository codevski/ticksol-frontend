import React from 'react';
import TextInput from '../../components/FormComponents/TextInput';
import { connect } from 'react-redux';
import axios from 'axios';
import { actionCreators } from "../../store/actions/venueActions";
import ButtonSave from '../../components/EventHandlers/ButtonSave';

class GeneralSettings extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            settingsData: [],
            deleted: false,
        };
        this.onDelete = this.onDelete.bind(this);

    }
    onDelete(){
        this.setState({deleted: !this.state.deleted});
    };

    onChangeString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onChangeString(action, test));
    };

    onUpdateString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onUpdateString(action, test));
    };

    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1'+ this.props.location.pathname)

        axios.get('http://128.199.124.144/api/settings')
            .then((res) => {{console.log(res.data.settings[0].company_name)}
                this.setState({settingsData: res.data.settings[0].company_name});
                this.onUpdateString('SETTINGS_COMP_NAME', res.data.settings[0].company_name);
            })
            .catch(function (err) {
                console.error('err', err);
            });
    }

    render(){
        const {settings} = this.props;

        return(
            <div className="col-md-10">
                <div className="row">
                    <TextInput label={ "Company Name" } col={ "col col-md-6" } value={ settings.companyName } onChange={this.onChangeString.bind(this, 'SETTINGS_COMP_NAME')}/>
                    {/*<TextInput label={ "Theme" }  col={ "col col-md-3" } value={ event.eventStart } style={ "half-size" } onChange={this.onChangeString.bind(this, 'EVENT_START')}/>*/}
                </div>
                <ButtonSave name={ "SAVE" } type="Settings"/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        settings: state.settings
    }};

export default connect(mapStateToProps)(GeneralSettings);
