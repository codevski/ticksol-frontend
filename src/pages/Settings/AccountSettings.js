import React from 'react';
import TextInput from '../../components/FormComponents/TextInput';
import ImageUploader from '../../components/FormComponents/ImageUploader';
import ButtonSave from "../../components/EventHandlers/ButtonSave";
import { connect } from 'react-redux';
import { actionCreators } from "../../store/actions/venueActions";

class AccountSettings extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }
    onChangeString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onChangeString(action, test));
    };

    render(){
        const { user } = this.props;
        console.log(user.userName);
        return(
            <div>
                <row className="col col-md-6">
                    <ImageUploader type="user"/>
                </row>
                <TextInput col="col-md-6" label="Name" value={ user.userName } disabled="disabled" />
                <TextInput col="col-md-6" label="Email" value={ user.userEmail } onChange={this.onChangeString.bind(this, 'USER_EMAIL')}/>
                <TextInput col="col-md-6" type="password" label="Update Password"  onChange={this.onChangeString.bind(this, 'USER_PASSWORD')}/>
                <ButtonSave name={ "SAVE" } type="Account"/>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }};

export default connect(mapStateToProps)(AccountSettings);
