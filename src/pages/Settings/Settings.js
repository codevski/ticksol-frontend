import React from 'react';
import HeaderTemplate from '../../components/HeaderComponents/index';
import { Route, Switch } from 'react-router-dom';
import GeneralSettings from './GeneralSettings';
import AccountSettings from './AccountSettings';

const HeaderItems = [
    {
        name: "General",
        link: "/settings"
    },
    {
        name: "Account",
        link: "/settings/account"
    }
];

class Settings extends React.Component{
    render(){
        return(
            <div>
                <HeaderTemplate data={ HeaderItems } />

                <div className="container">
                    <Switch>
                        <Route path="/settings/account" component={AccountSettings} />
                        <Route path="/settings" component={GeneralSettings} />
                    </Switch>
                </div>
            </div>
        )
    }
}

export default Settings;
