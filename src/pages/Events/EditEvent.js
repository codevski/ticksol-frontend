import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux'
import { actionCreators } from '../../store/actions/index'
import TextInput from '../../components/FormComponents/TextInput'
import TextArea from '../../components/FormComponents/TextArea'
import ImageUploader from '../../components/FormComponents/ImageUploader';
import Button from '../../components/FormComponents/Button.js';
import Buttons from '../../components/EventComponents/Buttons';
import ConfirmDelete from '../../components/EventHandlers/ConfirmDelete';
import DatePicker from '../../components/Date';
import DropDown from '../../components/FormComponents/DropDown';

class EditEvent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            eventData: [],
            deleted: false,
            eventID: '',
        };
        this.onDelete = this.onDelete.bind(this);

    }
    onDelete(){
        this.setState({deleted: !this.state.deleted});
    };

    onChangeString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onChangeString(action, test));
    };

    onUpdateInt = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onUpdateInt(action, test));
    };

    onUpdateString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onUpdateString(action, test));
    };

    componentDidMount() { console.log(this.props.location.pathname);
        // axios.get('http://128.199.124.144/ticksol/public/api/v1'+ this.props.location.pathname)
        axios.get('http://128.199.124.144/api' + this.props.location.pathname)
            .then((res) => {
                this.eventID = res.data.event.id;
                this.setState({eventData: res.data.event});
                this.onUpdateString('EVENT_NAME', res.data.event.name);
                this.onUpdateString('EVENT_VENUE', res.data.event.venues[0].name);
                this.onUpdateString('EVENT_START', res.data.event.venues[0].pivot.start_time);
                this.onUpdateString('EVENT_FINISH', res.data.event.venues[0].pivot.end_time);
                this.onUpdateString('EVENT_SEATING', res.data.event.category);
                this.onUpdateString('EVENT_PRICE', res.data.event.price);
                this.onUpdateString('EVENT_DESCRIPTION', res.data.description);
                this.onUpdateString('EVENT_DATE', res.data.event.venues[0].pivot.conduct_date);
                this.onUpdateString('EVENT_IMG', res.data.event.image_url);
            })
            .catch(function (err) {
                console.error('err', err);
            });
    }

    render(){
        const {event} = this.props;

        return(
            <div className="col-md-10">
                <div className="row">{console.log(event.eventVenue)}
                    <TextInput label={ "Event Name" } col={ "col col-md-6" } value={ event.eventName } onChange={this.onChangeString.bind(this, 'EVENT_NAME')}/>
                   {/* <TextInput label={ "Event Venue" } col={ "col col-md-6" } value={ event.eventVenue } onChange={this.onChangeString.bind(this, 'EVENT_VENUE')}/>*/}
                    <DropDown label={ "Event Venue" } value={ event.eventVenue }  onChange={this.onChangeString.bind(this, 'EVENT_VENUE')} />
                    <DatePicker col={ "col col-md-6" } selectedDays={ event.eventDate }/>
                    <TextInput label={ "Start Time" }  col={ "col col-md-3" } value={ event.eventStart } style={ "half-size" } onChange={this.onChangeString.bind(this, 'EVENT_START')}/>
                    <TextInput label={ "Finish Time" } col={ "col col-md-3" } value={ event.eventFinish } style={ " " } onChange={this.onChangeString.bind(this, 'EVENT_FINISH')}/>
                    <TextInput label={ "Seating Category" } col={ "col col-md-3" } value={ event.eventSeatingCat } onChange={this.onChangeString.bind(this, 'EVENT_SEATING')}/>
                    <TextInput label={ "Price" } col={ "col col-md-3" } value={ event.eventPrice } onChange={this.onChangeString.bind(this, 'EVENT_PRICE')}/>
                </div>
                <div className="row">
                    <TextArea label={ "Event Description" } col={ "col col-md-6" } rows={ "8" } value={ event.eventDescription } onChange={this.onChangeString.bind(this, 'EVENT_DESCRIPTION')}/>
                    <div className="imageUploader col col-md-6">
                        <ImageUploader type="events"/>
                    </div>
                </div>

                <div className="row formButtons ">
                    <Button buttonName={ "DELETE" } buttonStyle={ "btn-danger pull-left mRight10"} onClick={this.onDelete} />
                    <Buttons name="UPDATE" postID={this.eventID} add={'update'} onClick={this.onUpdate}/>
                </div>
                {
                    this.state.deleted? <ConfirmDelete type="events" linkTo="/events" name={event.eventName} postID={this.eventID} deletedStatus={this.onDelete}/> : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        event: state.event
    }};


export default connect(mapStateToProps)(EditEvent);
