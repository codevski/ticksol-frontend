import React from 'react';
import HeaderTemplate from '../../components/HeaderComponents/index';
import AddEvent from './AddEvent';
import EditEvent from './EditEvent';
import PerformGetRequest from './GetEvents';
import { Route, Switch } from 'react-router-dom';

const HeaderItems = [
    {
        name: "Events",
        link: "/events"
    },
    {
        name: "Add Event",
        link: "/events/add"
    }
];

class Events extends React.Component{
    render(){
        return(
            <div>
                <HeaderTemplate data={ HeaderItems } />
                    <div className="container">
                        <Switch>
                            <Route path="/events/add" component={AddEvent} />
                            <Route path="/events/:postID" component={EditEvent} />
                            <Route path="/events" component={PerformGetRequest} />
                        </Switch>
                    </div>
            </div>
        );
    }
}

export default Events;
