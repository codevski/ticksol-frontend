import React from 'react';
import TextInput from '../../components/FormComponents/TextInput';
import { connect } from 'react-redux';
import ImageUploader from '../../components/FormComponents/ImageUploader';
import TextArea from '../../components/FormComponents/TextArea'
import DatePicker from '../../components/Date';
import Buttons from '../../components/EventComponents/Buttons';
import DropDown from '../../components/FormComponents/DropDown';

const AddEvent = (props) => (

    <div className="col-md-10">
      <div className="row">

        <TextInput label={ "Event Name" } col={ "col col-md-6" } value={ props.event.eventName } onChange={props.eventName}/>
          <DropDown label={ "Event Venue" } value={ props.event.eventVenue } onChange={props.eventVenue}/>
       {/* <TextInput label={ "Event Venue" } col={ "col col-md-6" } value={ props.event.eventVenue } onChange={props.eventVenue}/>*/}
        <DatePicker col={ "col col-md-6" } />
        <TextInput label={ "Start Time" }  col={ "col col-md-3" } value={ props.event.eventStart } style={ "half-size" } onChange={props.eventStart}/>
        <TextInput label={ "Finish Time" } col={ "col col-md-3" } value={ props.event.eventFinish } style={ " " } onChange={props.eventFinish}/>
        <TextInput label={ "Seating Category" } col={ "col col-md-3" } value={ props.event.eventSeatingCat } onChange={props.eventSeatingCat}/>
        <TextInput label={ "Price" } col={ "col col-md-3" } value={ props.event.eventPrice } onChange={props.eventPrice}/>
      </div>
      <div className="row">
          <TextArea label={ "Event Description" } col={ "col col-md-6" } rows={ "8" } value={ props.event.eventDescription } onChange={props.eventDescription}/>
        <div className="imageUploader col col-md-6">
            <ImageUploader type="events"/>
        </div>
      </div>

        <Buttons name="SUBMIT" add={'add'}/>

    </div>
);

const mapStateToProps = (state) => {console.log(state);
    return {
        event: state.event,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        eventName: (evt) => {
            const action = {type: 'EVENT_NAME', text: evt.target.value};
            dispatch(action);
        },
        eventStart: (evt) => {
            const action = {type: 'EVENT_START', text: evt.target.value};
            dispatch(action);
        },
        eventFinish: (evt) => {
            const action = {type: 'EVENT_FINISH', text: evt.target.value};
            dispatch(action);
        },
        eventSeatingCat: (evt) => {
            const action = {type: 'EVENT_SEATING', text: evt.target.value};
            dispatch(action);
        },
        eventPrice: (evt) => {
            const action = {type: 'EVENT_PRICE', text: evt.target.value};
            dispatch(action);
        },
        eventVenue: (evt) => {
            const action = {type: 'EVENT_VENUE', text: evt.target.value};
            dispatch(action);
        },
        eventDescription: (evt) => {
            const action = {type: 'EVENT_DESCRIPTION', text: evt.target.value};
            dispatch(action);
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddEvent);