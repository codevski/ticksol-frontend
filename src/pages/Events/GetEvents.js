import React from 'react';
import axios from 'axios';
import ImageThumbnail from '../../components/ImageThumbnail';
import { Link } from 'react-router-dom';
import ConfirmDelete from '../../components/EventHandlers/ConfirmDelete';

class PerformGetRequest extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            eventData: [],
            deleted: false,
            eventID: '',
            eventName: '',
        };
        this.onDelete = this.onDelete.bind(this);
        this.deleteStatus = this.deleteStatus.bind(this);
    };

    onDelete(props, name){
        this.setState({deleted: !this.state.deleted});
        this.eventID = props;
        this.eventName = name;
    };

    deleteStatus() {
        axios.get('http://128.199.124.144/api/events')
            .then((res) => {
                this.setState({eventData: res.data.events})
            })
            .catch(function (err) {
                console.error('err', err);
            });
        this.setState({deleted: !this.state.deleted});
    };

    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/events')
            .then((res) => {
            console.log(res.data);
                this.setState({eventData: res.data.events})
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };
    render(){
        const style = {
            background: 'transparent',
            border: 'none'
        };
        return(
            <div>
                {this.state.eventData.map((event) => {
                    return(
                        <div key={event.id} className="venue">

                            <div className="thumbnail col-sm-6 col-md-4">{event.name}
                                <button className="pull-right" style={style} onClick={this.onDelete.bind(this, event.id, event.name)}>
                                    <i className="fa fa-trash-o red" aria-hidden="true" />
                                </button>
                                <Link to={'/events/'+event.id}>
                                    <ImageThumbnail name={event.name} src={event.image_url}/>
                                </Link>
                            </div>
                        </div>
                    );
                })}
                {
                    this.state.deleted? <ConfirmDelete type="events" linkTo="/events" name={this.eventName} postID={this.eventID} deletedStatus={this.deleteStatus}/> : null
                }
            </div>
        )
    };
}

export default PerformGetRequest;