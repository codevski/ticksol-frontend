import React from 'react';
import UpcomingEvents from '../../components/UpcomingEvent';
import TicketsWidget from '../../components/TicketsWidget';
import Todos from '../../components/Todo';
import { Avatar, Icon, Progress } from 'antd';
import axios from 'axios';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dashData: [],
        };
    };

    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/dashboard')
            .then((res) => {
                this.setState({dashData: res.data.metrics});
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };

    render() {
        const styles = {
            isoLabel: {
                color: 'rgb(255, 255, 255)',
            },
            isoStickerWidget: {
                backgroundColor: 'rgb(114, 102, 186)',
            },
            ant: {
                marginBottom: 16,
            },
        };
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-sm-6">
                            <div className="panel widget bg-primary" style={{ backgroundColor:'#2980b9'}}>
                                <div className="row row-table">
                                    <div className="col-xs-4 text-center bg-primary-dark pv-lg">
                                        <em className="fa fa-map-marker fontAwesome" style={{fontSize: '3.5em', marginTop: '35%', marginLeft: '30%'}}></em>
                                    </div>
                                    <div className="col-xs-8 pv-lg" style={{ backgroundColor: '#3498db', width: '62%',
                                        paddingTop: 15, paddingBottom: 15}}>
                                        <div className="h2 mt0" style={{marginTop: 0}}>{this.state.dashData.venue_count}</div>
                                        <div className="text-uppercase">Venues</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="panel widget bg-primary" style={{ backgroundColor:'#8e44ad'}}>
                                <div className="row row-table">
                                    <div className="col-xs-4 text-center bg-primary-dark pv-lg">
                                        <em className="fa fa-calendar-o fontAwesome" style={{fontSize: '3em', marginTop: '35%', marginLeft: '30%'}}></em>
                                    </div>
                                    <div className="col-xs-8 pv-lg" style={{ backgroundColor: '#9b59b6', width: '62%', paddingTop: 15, paddingBottom: 15}}>
                                        <div className="h2 mt0" style={{marginTop: 0}}>{this.state.dashData.event_count}</div>
                                        <div className="text-uppercase">Events</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="panel widget bg-primary" style={{ backgroundColor:'#16a085'}}>
                                <div className="row row-table">
                                    <div className="col-xs-4 text-center bg-primary-dark pv-lg">
                                        <em className="fa fa-pencil-square-o" style={{fontSize: '3.5em', marginTop: '35%', marginLeft: '30%'}}></em>
                                    </div>
                                    <div className="col-xs-8 pv-lg" style={{ backgroundColor: '#1abc9c', width: '62%', paddingTop: 15, paddingBottom: 15}}>
                                        <div className="h2 mt0" style={{marginTop: 0}}>{this.state.dashData.pending_venues}</div>
                                        <div className="text-uppercase">Drafts</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3 col-sm-6">
                            <div className="panel widget bg-primary" style={{ backgroundColor:'#2c3e50'}}>
                                <div className="row row-table">
                                    <div className="col-xs-4 text-center bg-primary-dark pv-lg">
                                        <em className="fa fa-desktop" style={{fontSize: '3em', marginTop: '35%', marginLeft: '30%'}}></em>
                                    </div>
                                    <div className="col-xs-8 pv-lg" style={{ backgroundColor: '#34495e', width: '62%', paddingTop: 15, paddingBottom: 15}}>
                                        <div className="h2 mt0" style={{marginTop: 0}}>{this.state.dashData.plugins_enabled}</div>
                                        <div className="text-uppercase">Plugins</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <UpcomingEvents data={this.state.dashData} />
                        <TicketsWidget />
                        <Todos />
                    </div>
                </div>
            </div>

        );
    }
}

export default Dashboard;