import React from 'react';
import SignIn from './SignIn';
import SignUp from './SignUp';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import Confirm from '../../components/EventHandlers/Confirm';
import { actionCreators } from '../../store/actions/index'

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            signUp: false,
            modal: false,
        };
        this.signUp = this.signUp.bind(this);
        this.signUpSend = this.signUpSend.bind(this);
        this.signIn = this.signIn.bind(this);
    }

    signUp(){
        this.setState({signUp: !this.state.signUp});
    };

    signIn(){
        const url = "http://128.199.124.144/api/login";
        const {user} = this.props;
        const { userName } = this.props;
        const { userEmail } = this.props;
        const { userLoggedIn } = this.props;
        const { userImage } = this.props;
        const { userID } = this.props;

        const data = {
            "password": user.userPassword,
            "email": user.userEmail,
        };

        axios.post(url, data)
            .then( (res ) => {

            if(res.data.status === 200){ console.log(res.data);
                userLoggedIn('true');
                userName(res.data.user[0].name);
                userEmail(res.data.user[0].email);
                userImage(res.data.user[0].image_url);
                userID(res.data.user[0].id);
                window.location.href = "/"

            }else{
                console.log("false");
            }
            console.log(user);
            })
            .catch(function (err) {
                console.error('err', err);
            });

    };

    signUpSend(){
        this.setState({modal: !this.state.modal});
        this.setState({signUp: !this.state.signUp});
        const {user} = this.props;
        const url = "http://128.199.124.144/api/register";

        const data = {
            "password": user.userPassword,
            "email": user.userEmail,
            "name": user.userName,
            "image_url": "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        };

        axios.post(url, data)
            .then( (res ) => {
                // File uploaded successfully
                // this.setState({signUp: !this.state.signUp});

                this.setState({modal: !this.state.modal});
                this.registerReset();

            })
            .catch(function (err) {
                console.error('err', err);
            });
    };

  render(){
    return(
      <div className="login">
        <div className="container-fluid">
          <div className="row clearfix">

            <div className="col-lg-9 col-md-8 col-xs-12">
              <div className="loginDetails">
                <h1>Tick<u>SOL</u></h1>
                <h3>Sign in to start your session</h3>
                <h5>Ticketing solutions made easy with TickSOL. RELIABLE | ROBUST | EASY.</h5>
              </div>
            </div>
            <div className="col-lg-3 col-md-4 col-xs-12">
              <div className="loginBox">
                <form className="col-md-12">
              {
                  this.state.signUp?  <SignUp/> : <SignIn />
              }
                    {
                        this.state.signUp?
                            <div>
                              <button type="button" className="btn btnSign" onClick={this.signUpSend}>Done</button>
                                <button type="button" className="btn btnReg" onClick={this.signUp}>Cancel</button>
                            </div>
                            :
                                <div>
                                  <Link to="/">
                                    <button type="button" className="btn btnSign" onClick={this.signIn}>Sign In</button>
                                  </Link>
                                    <button type="button" className="btn btnReg" onClick={this.signUp}>Sign up</button>
                                </div>
                    }
                    {
                        this.state.modal? <Confirm type="User" confirmType="Added" linkTo="/login" /> : null
                    }

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}

const mapStateToProps = (state) => {console.log(state);
    return {
        user: state.user
    }};
const onUpdateString = (action, test) => {
    const {dispatch} = this.props;
    dispatch(actionCreators.onUpdateString(action, test));
};
const mapDispatchToProps = (dispatch) => {
    return {
        userLoggedIn: (evt) => {
            const action = {type: 'USER_LOGGED_IN', text: evt};
            dispatch(action);
        },
        registerReset: () => {
            const action = {type: 'USER_RESET'};
            dispatch(action);
        },
        userEmail: (evt) => {
            const action = {type: 'USER_EMAIL', text: evt};
            dispatch(action);
        },
        userPassword: (evt) => {
            const action = {type: 'USER_PASSWORD', text: evt};
            dispatch(action);
        },
        userName: (evt) => {
            const action = {type: 'USER_NAME', text: evt};
            dispatch(action);
        },
        userImage: (evt) => {
            const action = {type: 'USER_IMAGE', text: evt};
            dispatch(action);
        },
        userID: (evt) => {
            const action = {type: 'USER_ID', text: evt};
            dispatch(action);
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
