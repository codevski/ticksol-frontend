import React from 'react';
import { connect } from 'react-redux';

const SignUp = (props) => (

            <div>
                <h1>SIGN UP</h1>
                    <div className="input-container">
                        <input type="text" id="Username" required="required" onChange={props.userName}/>
                        <label >Name</label>
                        <div className="bar"></div>
                    </div>
                    <div className="input-container">
                        <input id="Username" required="required" onChange={props.userEmail}/>
                        <label >Email</label>
                        <div className="bar"></div>
                    </div>
                    <div className="input-container">
                        <input type="password" id="Password" required="required" onChange={props.userPassword}/>
                        <label >Password</label>
                        <div className="bar"></div>
                    </div>
            </div>
        );
const mapStateToProps = (state) => {
    return {
        user: state.user
    }};

const mapDispatchToProps = (dispatch) => {
    return {
        userName: (evt) => {

            const action = {type: 'USER_NAME', text: evt.target.value};
            dispatch(action);
        },
        userEmail: (evt) => {
            const action = {type: 'USER_EMAIL', text: evt.target.value};
            dispatch(action);
        },
        userPassword: (evt) => {
            const action = {type: 'USER_PASSWORD', text: evt.target.value};
            dispatch(action);
        },
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(SignUp);