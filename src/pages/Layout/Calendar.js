import React from 'react';
import BigCalendar from 'react-big-calendar';
//import events from '../../events';
import 'react-big-calendar/lib/css/react-big-calendar.css'
import moment from 'moment';
import axios from 'axios';

let allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k])
BigCalendar.momentLocalizer(moment);

const url = 'http://128.199.124.144/api/venues/events/calendar';



class eventsCal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            calEvents: [{
                'title': 'Long Event',
                'start': new Date(2015, 3, 7),
                'end': new Date(2015, 3, 10)
            }]
        }
    }

    componentDidMount() {
        axios.get(url)
            .then( (res ) => {
                console.log("updated")
                //console.log(res.data.events)
                var newArray = this.state.calEvents.slice();
                var adate = new Date('Fri Oct 13 2017 12:00:00 GMT+1100 (AEDT)')
                //var from = $("#datepicker").val().split("-");
                console.log(adate.getDay());
                res.data.events.map((event, i) => {
                    newArray.push({
                        'title': event.title,
                        'start': new Date(event.date_YY, event.date_MM - 1, event.date_DD, event.start_time_HH,
                            event.start_time_MM, event.start_time_SS, 0),
                        'end': new Date(event.date_YY, event.date_MM - 1, event.date_DD, event.end_time_HH,
                            event.end_time_MM, event.end_time_SS, 0)
                    })
                    console.log(newArray);
                    this.setState({calEvents:newArray})
                })


                // this.setState({...this.state.calEvents, calEvents: [{
                //     'title': res.data.events[0].title,
                //     'start': new Date(2015, 3, 7),
                //     'end': new Date(2015, 3, 7)
                // }]})

                //window.location.href = "/addons"
                // File uploaded successfully
            })
            .catch(function (err) {
                console.error('err', err);
            });
    }
    render(){
        return (
            <div className="col-lg-12" style={{position: "fixed", width: '80%', height: '80%'}}>
                {console.log(this.state.calEvents)}
                <BigCalendar
                    {...this.props}
                    events={this.state.calEvents}
                    views={allViews}
                    step={60}
                    defaultDate={new Date(2017, 9, 16)}
                />
            </div>
        )
    }
}

export default eventsCal;