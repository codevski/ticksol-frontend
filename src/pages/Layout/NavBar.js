import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import axios from 'axios';
import 'antd/dist/antd.css';
import { Link, Route, Redirect } from 'react-router-dom';
import { Avatar, Badge, Menu, Dropdown, Icon } from 'antd';
import { connect } from 'react-redux';

class NavBar extends React.Component{
    constructor(props) {
        super(props);
        this.state = {value:"search"};
        this.eventID = null;
    }

    state = {
        venueData: []
    };
    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/eventvenue')
            .then((res) => {
                console.log(res.data.result);
                this.setState({venueData: res.data.result})
            })
            .catch(function (err) {
                console.error('err', err);
            });
    }
    updateState(element) {
        this.setState({value: element});
        console.log(element)
        this.eventID = element;
        console.log(this.eventID)
        this.setState({redirect: true});
    }
    handleOnClick = () => {
        // some action...
        // then redirect
        this.setState({redirect: true});
    };
    logout = () => {
        const { userLoggedOut } = this.props;
        userLoggedOut();
    };
    render(){
        const getOptions = (input) => {
            return axios.get(`http://128.199.124.144/api/eventvenue?q=${input}`)
                .then((res) => {
                    console.log(JSON.stringify(res.data.result));
                    return res.data.result;
                }).then((json) => {
                    /* console.log(JSON.stringify(json));*/
                    return { options: json };
                });
        }

        const menu = (
            <Menu>
                <Menu.Item key="0">
                    <a href="/settings/account">Profile</a>
                </Menu.Item>
                <Menu.Item key="1">
                    <a href="/settings">Settings</a>
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="3">
                    <a href="/login" onClick={this.logout.bind(this)}>Logout</a>
                </Menu.Item>
            </Menu>
        );

        if (this.state.redirect) {
            console.log(this.eventID)
            // return this.eventID;

            if(this.eventID.start_date != null) {

                return window.location.href="/events/" + this.eventID.id
            }
            else {
                return window.location.href="/venues/view/" + this.eventID.id
            }
        }

        return(
            <nav className="navbar navbar-default">
                <div className="container-fluid" style={{marginTop: 8 }}>
                    <div className="col-lg-3 col-md-4 col-xs-12">
                        <Select.Async
                            name="form-field-name"
                            placeholder="Search..."
                            noResultsText="No results"
                            value={this.state.value}
                            openOnClick={false}
                            // options={options}
                            valueKey="name"
                            loadOptions={getOptions}
                            onChange={this.updateState.bind(this)}
                            //onValueClick={this.updateState.bind(this)}
                        />
                    </div>
                    <div style={{float: 'right'}}>
                        {/*<Avatar>USER</Avatar>*/}
                        <Dropdown overlay={menu} trigger={['click']}>
                            <a className="ant-dropdown-link" href="#">
                                <Avatar src={this.props.user.userImage}/> {this.props.user.userName}</a>
                        </Dropdown>
                        {/*<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/> Demo*/}
                        {/*<span style={{ marginRight: 24 }}>*/}
                        {/*<Badge count={1}><Avatar shape="square" icon="user" /></Badge>*/}
                        {/*</span>*/}
                    </div>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userLoggedOut: () => {
            const action = {type: 'USER_RESET'};
            dispatch(action);
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);