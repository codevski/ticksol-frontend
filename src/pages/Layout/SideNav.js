import React, { Component } from 'react';
import MenuItem from '../../components/NavComponents/MenuItems';
import logo from '../../images/ticksol_final_trans_white.png';
import axios from 'axios';
import { connect } from 'react-redux';

class SideNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addonData: [],
            companyName: '',
        };
        /*this.onUpdateChecked = this.onUpdateChecked.bind(this);*/

        const url = 'http://128.199.124.144/api/plugins/';
        axios.get(url)
            .then( (res ) => {
                console.log("updated")
                // console.log(res.data[0].type)
                res.data.plugins.map((addons) => {
                    if(addons.is_enabled == true) {
                        console.log(addons.name)
                        this.setState({
                            addonData: [...this.state.addonData, addons.name]
                        })
                        // this.setState({addonData: addon})
                    }
                })

                // File uploaded successfully
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };
    componentDidMount() {
        const url = 'http://128.199.124.144/api/settings/1';
        axios.get(url)
            .then( (res ) => {
                this.setState({companyName: res.data.setting.company_name});
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };
    render() {
        return    (
            <div className="SideBar-Container">
                <div className="container-fluid">
                    <div className="row">
                        <h1 className="company-title">{this.state.companyName}</h1>
                    </div>
                </div>

                <MenuItem addonData={this.state.addonData}/>

                <div className="Logo">
                    <img src={logo} alt="l" className="Logo"/>
                </div>
            </div>
        );
    }
}


export default SideNav;