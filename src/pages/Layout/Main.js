import React, { Component } from 'react';
import '../../App.css';
import SideNav from '../../pages/Layout/SideNav';
import Dashboard from '../../pages/Dashboard/Dashboard';
import Venues from '../../pages/Venues/Venues';
import NavBar from '../../pages/Layout/NavBar';
import Events from '../Events/Events';
import Settings from '../../pages/Settings/Settings';
import Addons from '../../pages/Addons/index';
import MyCalendar from '../../pages/Layout/Calendar'
import {
    Route,
    Switch,
} from 'react-router-dom';

const Main = () => (
    <div className="Main">
        <NavBar />
            <SideNav />
                <div id="page-wrapper">
                    <Switch>
                        <Route exact path="/" component={Dashboard} />
                        <Route path="/events" component={Events} onEnter={this.handleEnter} />
                        <Route path="/venues" component={Venues}/>
                        <Route path="/addons/calendar" component={MyCalendar}/>
                        <Route path="/addons" component={Addons}/>
                        <Route path="/settings" component={Settings}/>
                    </Switch>
                </div>
    </div>
);

export default Main;