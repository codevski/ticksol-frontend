import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux'
import { actionCreators } from '../../store/actions/index'
import TextInput from '../../components/FormComponents/TextInput'
import CheckBox from '../../components/FormComponents/CheckBox'
import TextArea from '../../components/FormComponents/TextArea'
import ImageUploader from '../../components/FormComponents/ImageUploader';
import Buttons from '../../components/VenueComponents/Buttons';
import Button from '../../components/FormComponents/Button.js';
import ConfirmDelete from '../../components/EventHandlers/ConfirmDelete';

class EditVenue extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            venueData: [],
            deleted: false,
            venueID: '',
        };
        this.onDelete = this.onDelete.bind(this);

    }
    onDelete(){
        this.setState({deleted: !this.state.deleted});
    };

    onChangeChecked = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onChangeChecked(action, test));
    };

    onChangeInt = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onChangeInt(action, test));
    };

    onChangeString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onChangeString(action, test));
    };

    onUpdateChecked = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onUpdateChecked(action, test));
    };

    onUpdateInt = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onUpdateInt(action, test));
    };

    onUpdateString = (action, test) => {
        const {dispatch} = this.props;
        dispatch(actionCreators.onUpdateString(action, test));
    };

    componentDidMount() {

        axios.get('http://128.199.124.144/api'+ this.props.location.pathname)
            .then((res) => { console.log(this.props.location.pathname)
                console.log( this.props.location.pathname);
                this.venueID = res.data.venue.id;
                this.setState({venueData: res.data});

                this.onUpdateString('VENUE_NAME', res.data.venue.name);
                this.onUpdateInt('VENUE_CAPACITY', res.data.venue.capacity);
                this.onUpdateString('VENUE_ADDRESS_ONE', res.data.venue.address_line_1);
                this.onUpdateString('VENUE_ADDRESS_TWO', res.data.venue.address_line_2);
                this.onUpdateString('VENUE_CITY', res.data.venue.city);
                this.onUpdateString('VENUE_STATE', res.data.venue.state);
                this.onUpdateString('VENUE_POSTCODE', res.data.venue.postcode);
                this.onUpdateString('VENUE_COUNTRY', res.data.venue.country);
                this.onUpdateString('VENUE_EMAIL', res.data.venue.email);
                this.onUpdateString('URL', res.data.venue.website_url);
                this.onUpdateString('VENUE_DESCRIPTION', res.data.venue.description);
                this.onUpdateString('VENUE_CONTACT_NO', res.data.venue.contact_num);
                this.onUpdateChecked('VENUE_WHEELCHAIR', res.data.venue.wheelchair_access);
                this.onUpdateChecked('VENUE_PARKING', res.data.venue.parking);
                this.onUpdateChecked('VENUE_TRAINS', res.data.venue.trains);
                this.onUpdateChecked('VENUE_TRAMS', res.data.venue.trams);
                this.onUpdateChecked('VENUE_BUSSES', res.data.venue.busses);
                this.onUpdateChecked('VENUE_TAXI_UBER', res.data.venue.taxi_uber);
                this.onUpdateChecked('VENUE_RESTAURANT', res.data.venue.restaurants);
                this.onUpdateChecked('VENUE_PUBS', res.data.venue.pubs);

            })
            .catch(function (err) {
                console.error('err', err);
            });
    }

    render(){
        const {venue} = this.props;

        return(
            <div className="col-md-10">
                <div className="row">
                    <TextInput label={ "Venue Name" } col={ "col col-md-6" } value={ venue.venueName } onChange={this.onChangeString.bind(this, 'VENUE_NAME')}/>
                    <TextInput label={ "Email" } col={ "col col-md-6" } placeholder="john.smith@example.com" value={ venue.venueEmail } onChange={this.onChangeString.bind(this, 'VENUE_EMAIL')}/>
                    <TextInput label={ "Address Line 1" } col={ "col col-md-6" } value={ venue.venueAddressOne } onChange={this.onChangeString.bind(this, 'VENUE_ADDRESS_ONE')}/>
                    <TextInput label={ "Address Line 2" } col={ "col col-md-6" } value={ venue.venueAddressTwo } onChange={this.onChangeString.bind(this, 'VENUE_ADDRESS_TWO')}/>
                    <TextInput label={ "City" } col={ "col col-md-6" } value={ venue.venueCity } onChange={this.onChangeString.bind(this, 'VENUE_CITY')}/>
                    <TextInput label={ "State" }  col={ "col col-md-3" } value={ venue.venueState } style={ "half-size" } onChange={this.onChangeString.bind(this, 'VENUE_STATE')}/>
                    <TextInput label={ "Postcode" } col={ "col col-md-3" } value={ venue.venuePostcode } style={ " " } onChange={this.onChangeString.bind(this, 'VENUE_POSTCODE')}/>
                    <TextInput label={ "Country" } col={ "col col-md-6" } value={ venue.venueCountry } onChange={this.onChangeString.bind(this, 'VENUE_COUNTRY')}/>
                    <TextInput label={ "Contact Number" } col={ "col col-md-6" } value={ venue.venueContactNumber } onChange={this.onChangeInt.bind(this, 'VENUE_CONTACT_NO')}/>
                    <TextInput label={ "Venue Capacity" } col={ "col col-md-6" } value={ venue.venueCapacity } onChange={this.onChangeInt.bind(this, 'VENUE_CAPACITY')}/>
                    <CheckBox label={ "Parking" } checked={ venue.venueParking } onChange={this.onChangeChecked.bind(this, 'VENUE_PARKING')}/>
                    <CheckBox label={ "Trains" } checked={ venue.venueTrains } onChange={this.onChangeChecked.bind(this, 'VENUE_TRAINS')}/>
                    <CheckBox label={ "Trams" } checked={ venue.venueTrams } onChange={this.onChangeChecked.bind(this, 'VENUE_TRAMS')}/>
                    <CheckBox label={ "Busses" } checked={ venue.venueBusses } onChange={this.onChangeChecked.bind(this, 'VENUE_BUSSES')}/>
                    <CheckBox label={ "Taxi/Uber" } checked={ venue.venueTaxiUber } onChange={this.onChangeChecked.bind(this, 'VENUE_TAXI_UBER')}/>
                    <CheckBox label={ "Pubs/Bars" } checked={ venue.venuePubs } onChange={this.onChangeChecked.bind(this, 'VENUE_PUBS')}/>

                    <TextArea label={ "Venue Description" } col={ "col col-md-6" } rows={ "8" } value={ venue.venueDescription } onChange={this.onChangeString.bind(this, 'VENUE_DESCRIPTION')}/>
                    <div className="imageUploader col col-md-6">
                        <ImageUploader type="venues"/>
                    </div> </div>
                <Button buttonName={ "DELETE" } buttonStyle={ "btn-danger pull-left mRight10"} onClick={this.onDelete} />
                <Buttons name="UPDATE" postID={this.venueID} add={'update'} onClick={this.onUpdate}/>
                {
                    this.state.deleted? <ConfirmDelete type="venues" linkTo="/venues" name={venue.venueName} postID={"view/"+this.venueID} deletedStatus={this.onDelete}/> : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {console.log(state);
    return {
        venue: state.venue
    }};

export default connect(mapStateToProps)(EditVenue);
