import React from 'react';
import TextInput from '../../components/FormComponents/TextInput';
import CheckBox from  '../../components/FormComponents/CheckBox';
import { connect } from 'react-redux';
import ImageUploader from '../../components/FormComponents/ImageUploader';
import TextArea from '../../components/FormComponents/TextArea';
import { Confirm } from "../../components/EventHandlers/Cancel";
import Buttons from '../../components/VenueComponents/Buttons';
import Button from '../../components/FormComponents/Button';
import ButtonSave from '../../components/EventHandlers/ButtonSave';

const AddVenue = (props) => (

    <div className="col-md-10">
        <div className="row">
            <TextInput label={ "Venue Name" } col={ "col col-md-6" } value={ props.venueName } onChange={props.venueNameChanged}/>
            <TextInput label={ "Email" } col={ "col col-md-6" } placeholder="john.smith@example.com" value={ props.venueEmail } onChange={props.venueEmailChanged}/>
            <TextInput label={ "Address Line 1" } col={ "col col-md-6" } value={ props.venueAddressOne } onChange={props.venueAddressOneChanged}/>
            <TextInput label={ "Address Line 2" } col={ "col col-md-6" } value={ props.venueAddressTwo } onChange={props.venueAddressTwoChanged}/>
            <TextInput label={ "City" } col={ "col col-md-6" } value={ props.venueCity } onChange={props.venueCityChanged}/>
            <TextInput label={ "State" }  col={ "col col-md-3" } value={ props.venueState } style={ "half-size" } onChange={props.venueStateChanged}/>
            <TextInput label={ "Postcode" } col={ "col col-md-3" } value={ props.venuePostcode } style={ " " } onChange={props.venuePostcodeChanged}/>
            <TextInput label={ "Country" } col={ "col col-md-6" } value={ props.venueCountry } onChange={props.venueCountryChanged}/>
            <TextInput label={ "Contact Number" } col={ "col col-md-6" } value={ props.venueContactNumber } onChange={props.venueContactNumberChanged}/>

            <TextInput label={ "Venue Capacity" } col={ "col col-md-6" } value={ props.venueCapacity } onChange={props.venueCapacityChange}/>
            <CheckBox label={ "Parking" } checked={ props.venueParking } onChange={ props.venueParkingChange }/>
            <CheckBox label={ "Trains" } checked={ props.venueTrains } onChange={ props.venueTrainsChange }/>
            <CheckBox label={ "Trams" } checked={ props.venueTrams } onChange={ props.venueTramsChange }/>
            <CheckBox label={ "Busses" } checked={ props.venueBusses } onChange={ props.venueBussesChange }/>
            <CheckBox label={ "Taxi/Uber" } checked={ props.venueTaxiUber } onChange={ props.venueTaxiUberChange }/>
            {/*<CheckBox label={ "Restaurants" } checked={ props.venueRestaurants } onChange={ props.venueRestaurantsChange }/>*/}
            <CheckBox label={ "Pubs/Bars" } checked={ props.venuePubs } onChange={ props.venuePubsChange }/>

            <TextArea label={ "Venue Description" } col={ "col col-md-6" } rows={ "8" } value={ props.venueDescription } onChange={props.venueDescriptionChange}/>
            <div className="imageUploader col col-md-6">
                <ImageUploader type="venues"/>
            </div> </div>
            <ButtonSave name={ "SAVE" } type="Save" />
        <Buttons name="SUBMIT" add={'add'}/>

    </div>
);

const mapStateToProps = (state) => {console.log(state);
    return {
        venues: state.venue,
        venueName: state.venue.venueName,
        venueAddressOne: state.venue.venueAddressOne,
        venueAddressTwo: state.venue.venueAddressTwo,
        venueCity: state.venue.venueCity,
        venueState: state.venue.venueState,
        venuePostcode: state.venue.venuePostcode,
        venueCountry: state.venue.venueCountry,
        venueContactNumber: state.venue.venueContactNumber,
        venueEmail: state.venue.venueEmail,
        venueCapacity: state.venue.venueCapacity,
        venueWheelchair: state.venue.venueWheelchair,
        venueParking: state.venue.venueParking,
        venueTrains: state.venue.venueTrains,
        venueTrams: state.venue.venueTrams,
        venueBusses: state.venue.venueBusses,
        venueTaxiUber: state.venue.venueTaxiUber,
        venueRestaurants: state.venue.venueRestaurants,
        venuePubs: state.venue.venuePubs,
        venueReset: state.venue.venueReset,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueNameChanged: (evt) => {

            const action = {type: 'VENUE_NAME', text: evt.target.value};
            dispatch(action);
        },
        venueAddressOneChanged: (evt) => {
            const action = {type: 'VENUE_ADDRESS_ONE', text: evt.target.value};
            dispatch(action);
        },
        venueAddressTwoChanged: (evt) => {
            const action = {type: 'VENUE_ADDRESS_TWO', text: evt.target.value};
            dispatch(action);
        },
        venueCityChanged: (evt) => {
            const action = {type: 'VENUE_CITY', text: evt.target.value};
            dispatch(action);
        },
        venueStateChanged: (evt) => {
            const action = {type: 'VENUE_STATE', text: evt.target.value};
            dispatch(action);
        },
        venuePostcodeChanged: (evt) => {
            const action = {type: 'VENUE_POSTCODE', text: evt.target.value};
            dispatch(action);
        },
        venueCountryChanged: (evt) => {
            const action = {type: 'VENUE_COUNTRY', text: evt.target.value};
            dispatch(action);
        },
        venueContactNumberChanged: (evt) => {
            const action = {type: 'VENUE_CONTACT_NO', text: evt.target.value};
            dispatch(action);
        },
        venueEmailChanged: (evt) => {
            const action = {type: 'VENUE_EMAIL', text: evt.target.value};
            dispatch(action);
        },
        venueCapacityChange: (evt) => {
            const action = {type: 'VENUE_CAPACITY', number: evt.target.value};
            dispatch(action);
        },
        venueWheelchairChange: (evt) => {
            const action = {type: 'VENUE_WHEELCHAIR', checked: evt.target.checked};
            dispatch(action);
        },
        venueParkingChange: (evt) => {
            const action = {type: 'VENUE_PARKING', checked: evt.target.checked};
            dispatch(action);
        },
        venueTrainsChange: (evt) => {
            const action = {type: 'VENUE_TRAINS', checked: evt.target.checked};
            dispatch(action);
        },
        venueTramsChange: (evt) => {
            const action = {type: 'VENUE_TRAMS', checked: evt.target.checked};
            dispatch(action);
        },
        venueBussesChange: (evt) => {
            const action = {type: 'VENUE_BUSSES', checked: evt.target.checked};
            dispatch(action);
        },
        venueTaxiUberChange: (evt) => {
            const action = {type: 'VENUE_TAXI_UBER', checked: evt.target.checked};
            dispatch(action);
        },
        venueRestaurantsChange: (evt) => {
            const action = {type: 'VENUE_RESTAURANT', checked: evt.target.checked};
            dispatch(action);
        },
        venuePubsChange: (evt) => {
            const action = {type: 'VENUE_PUBS', checked: evt.target.checked};
            dispatch(action);
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddVenue);