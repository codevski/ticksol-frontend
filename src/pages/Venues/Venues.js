import React from 'react';
import PerformGetRequest from './GetVenues';
import EditVenue from './EditVenue';
import HeaderTemplate from '../../components/HeaderComponents/index';
import AddVenue from './AddVenue';
import { Route, Switch } from 'react-router-dom';
import PendingVenues from './PendingVenues';

const HeaderItems = [
    {
        name: "Venues",
        link: "/venues"
    },
    {
        name: "Add Venue",
        link: "/venues/add"
    },
    {
        name: "Pending",
        link: "/venues/pending"
    }
];

class Venues extends React.Component{
  render(){
    return(
        <div>
            <HeaderTemplate data={ HeaderItems } />

            <div className="container">
                <Switch>
                    <Route path="/venues/add" component={AddVenue} />
                    <Route path="/venues/pending" component={PendingVenues} />
                    <Route path="/venues/view/:postID" component={EditVenue} />
                    <Route path="/venues" component={PerformGetRequest} />
                </Switch>
            </div>
        </div>
    )
  }
}
export default Venues;
