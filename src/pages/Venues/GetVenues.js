import React from 'react';
import axios from 'axios';
import ImageThumbnail from '../../components/ImageThumbnail';
import { Link } from 'react-router-dom';
import ConfirmDelete from '../../components/EventHandlers/ConfirmDelete';

class PerformGetRequest extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            venueData: [],
            isDel: false,
            deleted: false,
            venueID: '',
            venueName: '',
        };
        this.onDelete = this.onDelete.bind(this);
        this.deleteStatus = this.deleteStatus.bind(this);
    };

    onDelete(props, name){
        this.setState({deleted: !this.state.deleted});
        this.venueID = props;
        this.venueName = name;

    };

    deleteStatus() {

        axios.get('http://128.199.124.144/api/venues/view')
            .then((res) => {
                console.log(res.data.venues);
                this.setState({venueData: res.data.venues})
            })
            .catch(function (err) {
                console.error('err', err);
            });
        this.setState({deleted: !this.state.deleted});
    }

    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/venues/view')
            .then((res) => {
                console.log(res.data.venues);
                this.setState({venueData: res.data.venues})
            })
            .catch(function (err) {
                console.error('err', err);
            });
    }

    // componentDidUpdate() {
    //     if(this.state.deleted) {
    //         axios.get('http://localhost:3005/venues')
    //             .then((res) => {
    //                 console.log(res.data);
    //                 this.setState({venueData: res.data})
    //             })
    //             .catch(function (err) {
    //                 console.error('err', err);
    //             });
    //     }
    //    else {
    //         console.log("DELETED BOOLEAN");
    //         console.log(this.state.deleted);
    //     }
    // }

    render(){
        const style = {
            background: 'transparent',
            border: 'none'
        }; console.log(this.state.venueData);
        return(
            <div>
                {this.state.venueData.map((venue) => {
                    return(
                        <div key={venue.id} className="venue">
                            { venue.pending? null :
                                <div className="thumbnail col-sm-6 col-md-4">{venue.name}
                                    <button className="pull-right" style={style}
                                            onClick={this.onDelete.bind(this, venue.id, venue.name)}>

                                        <i className="fa fa-trash-o red" aria-hidden="true"/>
                                    </button>
                                    <Link to={'/venues/view/' + venue.id}>
                                        <ImageThumbnail name={venue.name} src={venue.website_url}/>
                                    </Link>
                                </div>
                            }
                        </div>
                    );
                })}
                {
                    this.state.deleted? <ConfirmDelete type="venues" linkTo="/venues" name={this.venueName} postID={"view/"+this.venueID} deletedStatus={this.deleteStatus}/> : null
                }
            </div>
        )
    };
}

export default PerformGetRequest;