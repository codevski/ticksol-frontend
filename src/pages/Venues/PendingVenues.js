import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom'

class PendingVenues extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            venueData: [],
        };
        this.onDelete = this.onDelete.bind(this);
        this.deleteStatus = this.deleteStatus.bind(this);
    };

    componentDidMount() {
        axios.get('http://128.199.124.144/api/venues/view')
            .then((res) => {
                this.setState({venueData: res.data.venues})
            })
            .catch(function (err) {
                console.error('err', err);
            });
    }

    onDelete(props, name){
        this.setState({deleted: !this.state.deleted});
        this.venueID = props;
        this.venueName = name;

    };

    deleteStatus() {
        axios.get('http://128.199.124.144/api/venues/view')
            .then((res) => {
                this.setState({venueData: res.data.venues})
            })
            .catch(function (err) {
                console.error('err', err);
            });
        this.setState({deleted: !this.state.deleted});
    }

    render(){
        return(
            <div>
                <div className="container">
                    <div className="col col-lg-10">
                        <table>
                            <tbody>
                            <tr>
                                <th>Venue</th>
                                <th>Status</th>
                            </tr>
                            {this.state.venueData.map((pending) => {
                                return(
                                    <tr key={pending.id}>
                                        {
                                            pending.pending? <td>{pending.name}</td> : null
                                        }
                                        {
                                            pending.pending? <td> <Link to={'/venues/view/'+pending.id}>Incomplete</Link> </td> : null
                                        }
                                    </tr>
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        );
    }
}

export default PendingVenues;