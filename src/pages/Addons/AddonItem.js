import React from 'react';

export const AddonItem = (props) =>(

        <tr >
            <td className="addon">{props.type}</td>
            <td>
                <label className="switch">
                    <input type="checkbox" defaultChecked={props.checked}  onChange={props.onChange} />
                    <span className="slider round" />
                </label>
            </td>
        </tr>
);

