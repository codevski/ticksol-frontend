import React from 'react';
import { AddonItem } from './AddonItem';
import axios from 'axios';
import { connect } from 'react-redux';
import { AddAddonHandle } from '../../components/EventHandlers/AddonAPI';

class Addons extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            addonData: [],
            is_enabled: '',
        };
        /*this.onUpdateChecked = this.onUpdateChecked.bind(this);*/
    };

    onUpdateChecked = (e) => {

        if(this.state.addonData[e.id - 1].is_enabled === 1){
            this.state.addonData[e.id - 1].is_enabled = 0;
        }else{
            this.state.addonData[e.id - 1].is_enabled = 1;
        };


        AddAddonHandle(e);
    };

    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/plugins')
            .then((res) => {
                console.log(res.data.plugins)
                this.setState({addonData: res.data.plugins});
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };
    render(){

        return(
            <div>
                <div className="container">
                    <div className="col col-lg-10">
                        <table>
                            <tbody>
                            <tr>
                                <th className="addon-title">Add-on</th>
                                <th className="addon-title">Enable/Disable</th>
                            </tr>
                            {this.state.addonData.map((addon) => { {console.log(addon.is_enabled)}
                                return(
                                    <AddonItem key={addon.id} type={addon.name} checked={addon.is_enabled} onChange={this.onUpdateChecked.bind(this, addon)} />

                                );
                            })}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default Addons;