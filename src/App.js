import React, { Component } from 'react';
import './App.css';
import Login from './pages/Login/Login';
import Main from './pages/Layout/Main';
import { connect } from 'react-redux';

import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store/index'

const App = () => (
    <Provider store={store}>
        <Router>
            <div className="App"> {console.log(store.getState().user.userLoggedIn)}
                {
                    store.getState().user.userLoggedIn? <Route path="/" component={Main} /> : <Route path="/" component={Login} />
                }
                {/*<Switch>
                    <Route path="/login" component={Login} />
                    <Route path="/" component={Main} />
                </Switch>*/}
            </div>
        </Router>
    </Provider>
);


export default App;
