import React from 'react';

class Todos extends React.Component{
    constructor() {
        super()
        this.state = {
            nextTodo: {
                name: '',
                description: ''
            },
            list: [
                {name: 'Confirm with ABC Security', description: 'test1d'},
                {name: 'Extra required at Gate B', description: 'test2d'},
                {name: 'Order VIP shuttle service for band members', description: 'test2d'},
            ]
        }
    }

    updateTodo(field, event){
        console.log('UPDATE TODO: ' + field + '==' + event.target.value)
        let nextTodo = Object.assign({}, this.state.nextTodo)
        nextTodo[field] = event.target.value

        this.setState({
            nextTodo: nextTodo
        })
    }

    addTodo(event){
        console.log('ADD TODO: ' + JSON.stringify(this.state.nextTodo))
        let list = Object.assign([], this.state.list)
        list.push(this.state.nextTodo)
        this.setState({
            list: list,
            nextTodo: {
                name: '',
                description: ''
            }
        })
    }
    render(){
        return(
            <div className="col-lg-4 col-md-6">
                <div className="panel panel-warning" style={{borderColor: '#34495e'}}>
                    <div className="panel-heading" style={{
                        backgroundColor: '#34495e',
                        color: 'white',
                        borderColor: '#34495e'
                    }}>
                        <div className="row">
                            <div className="col-xs-5">
                                Todo List
                            </div>
                            <div className="col-xs-9 text-right">

                            </div>
                        </div>
                    </div>
                    <div className="col-md-12 todo" >
                        <ol>
                            { this.state.list.map((item, i) => {
                                return <li key={i}>{item.name}</li>
                            })}
                        </ol>
                        <input value={this.state.nextTodo.name} onChange={this.updateTodo.bind(this, 'name')} className="form-control" type="text" placeholder="Name" /> <br />
                        <input value={this.state.nextTodo.description} onChange={this.updateTodo.bind(this, 'description')} className="form-control" type="text" placeholder="Description" /><br/>
                        <button className="btn btn-primary pull-right" onClick={this.addTodo.bind(this)}>Add Todo</button>
                    </div>
                    <div className="clearfix"></div>

                </div>
            </div>
        );
    }
}

export default Todos;