import React from 'react';
import DayPicker from 'react-day-picker';
import { connect } from 'react-redux';

import 'react-day-picker/lib/style.css';

class DatePicker extends React.Component {
    state = {
        selectedDay: null,
    };
    handleDayClick = (day, { selected }) => {
        this.setState({
            selectedDay: selected ? undefined : day,
        });
        this.props.eventDate(day);
    };
    render() {
       /* if(this.props.value !== null ) {
            console.log(this.props.value);
            this.state.selectedDay = this.props.value;
            console.log(this.state.selectedDay);
        }*/
        const { selectedDay } = this.state;

        return (
            <div className="col col-md-6">
                <DayPicker
                    numberOfMonths={2}
                    selectedDays={selectedDay}
                    onDayClick={this.handleDayClick}
                />
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        date: state.eventDate,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        eventDate: (evt) => {
            const action = {type: 'EVENT_DATE', text: evt};
            dispatch(action);
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DatePicker);