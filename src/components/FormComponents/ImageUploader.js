import React from 'react';
import Dropzone from 'react-dropzone';
import axios from 'axios';
import { connect } from 'react-redux';

class ImageUploader extends React.Component {
    constructor() {
        super();
        this.state = {
            image:'',
            accepted: [],
            files: [],
        }
    }

    onDrop(files) {
        this.setState({
            files
        });
        const image = files[0];
        const url = 'https://api.cloudinary.com/v1_1/drysnqhbw/image/upload';
        const fd = new FormData();
        fd.append("file", image);
        fd.append("tags", `image, outdoor, park`);
        fd.append("upload_preset", "fwwggfug");
        fd.append("api_key", "614581681792769");
        fd.append("timestamp", (Date.now() / 1000) | 0);
        const config = {
            headers: { "X-Requested-With": "XMLHttpRequest" },
            onUploadProgress: function(progressEvent) {
                // Do something with the native progress event
            }
        };

        axios.post(url, fd, config)
            .then( (res ) => {
                // File uploaded successfully

                this.setState({image: res.data.secure_url});
                if(this.props.type === "venues"){
                    this.props.addVenueImageUrl(res);
                }else if(this.props.type === 'user'){
                    this.props.addProfileImageUrl(res);
                }else{
                    this.props.addEventImageUrl(res);
                }

            })
            .catch(function (err) {
                console.error('err', err);
            });
    }

    render() {
        const overlayStyle ={
            width: '100%',
            height: '176px',
            borderWidth: '2px',
            borderColor: 'rgb(102, 102, 102)',
            borderStyle: 'dashed',
            borderRadius: '5px',
        };
        return (
            <section>
                <div className="dropzone imageUpload">
                    <Dropzone
                        style={ overlayStyle }
                        accept="image/jpeg, image/png"
                        onDrop={this.onDrop.bind(this)}
                        /*onDrop={(accepted, rejected) => { this.onTest.bind({ accepted, rejected }); }}*/
                    >
                        <div className="dropzoneImage">
                            {this.state.image ? (
                                <img className="imageThumb" src={ this.state.image } />
                            ) : (
                                <i className="fa fa-cloud-upload fa-5x" aria-hidden="true" />
                            )}
                        </div>
                    <div className="text">
                        <p>Drag an image, or click to select file to upload.</p>
                    </div>
                    </Dropzone>
                </div>
                <aside>
                    {/*<ul>
                        {
                            this.state.files.map(f => <li className="listImageName" key={f.name}>Uploaded file: {f.name}</li>)
                        }
                    </ul>*/}
                </aside>
            </section>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        url: state.venue.url,
        eventURL: state.event.eventImgURL,
        settingsImage: state.user.userImage,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addVenueImageUrl: (evt) => {
            const action = {type: 'URL', text: evt.data.secure_url};
            dispatch(action);
        },
        addEventImageUrl: (evt) => {
            const action = {type: 'EVENT_IMG', text: evt.data.secure_url};
            dispatch(action);
        },
        addProfileImageUrl: (evt) => {
            const action = {type: 'USER_IMAGE', text: evt.data.secure_url};
            dispatch(action);
        },
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ImageUploader);