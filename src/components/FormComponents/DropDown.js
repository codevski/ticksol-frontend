import React from 'react';
import { connect } from 'react-redux';
import axios from 'axios';

class DropDown extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            eventData: [],
        };

    }



    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/venues/view')
            .then((res) => {
                this.setState({eventData: res.data.venues});
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };

    render(){
        return(
            <div className="col col-md-6">
                <div className={"form-group"}>
                    <label> {this.props.label} </label>
                        <select value={this.props.value} className="form-control" onChange={ this.props.onChange }>
                            <option value='' >{this.props.value}</option>
                            {this.state.eventData.map((venue) => {
                                return(
                                    <option key={venue.id} value={venue.id} >{venue.name}</option>

                                );
                            })}
                        </select>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        event: state.event,
    }
};

export default connect(mapStateToProps)(DropDown);