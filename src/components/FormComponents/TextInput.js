import React from 'react'

const TextInput = (props) => (
    <div className={ props.col } >
        <div className={"form-group " + props.style}>
            <label>
                {props.label}
            </label>
                <input
                    type={props.type}
                    className="form-control"
                    value={ props.value }
                    onChange={ props.onChange }
                    disabled={props.disabled}
                />
        </div>
    </div>
);

export default TextInput;