import React from 'react'

const CheckBox = (props) => (
    <div className="col col-md-2">
        <div className="form-group">
            <input type="checkbox" defaultChecked={props.checked}  onChange={props.onChange} aria-label="..." />
            <label>{props.label}</label>
        </div>
    </div>
);

export default CheckBox;