import React from 'react';

const TextArea = (props) => (
    <div className={ props.col } >
        <div className={"form-group " + props.style}>
            <label>
                {props.label}
            </label>

            <textarea
                type="text"
                className="form-control"
                value={ props.value }
                rows={ props.rows }
                onChange={ props.onChange }
            />

        </div>
    </div>
);

export default TextArea;