import React from 'react'

const Button = (props) => (
    <div>
        <button className={"btn " + props.buttonStyle} onClick={ props.onClick }>
            { props.buttonName }
        </button>
    </div>
);

export default Button;