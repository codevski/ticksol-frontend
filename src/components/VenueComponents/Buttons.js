import React from 'react';
import Button from '../../components/FormComponents/Button.js';
import { AddVenueHandle, EditVenueHandle } from '../../components/EventHandlers/TickSolAPI';
import ConfirmExit from "../../components/EventHandlers/Cancel";
import Confirm from '../../components/EventHandlers/Confirm';
import { connect } from 'react-redux';

export class Buttons extends React.Component{
    constructor(){
        super();
        this.state= {
            cancelled: false,
            added: false,
            updated: false,
        };
        this.OnCancel = this.OnCancel.bind(this);
        this.onAdd = this.onAdd.bind(this);
    };

    OnCancel(){
        this.setState({cancelled: !this.state.cancelled});
    }

    onAdd(){
        if(this.props.add === 'add') {
            this.setState({added: !this.state.added});
            AddVenueHandle(this.props)
        }else{
            this.setState({updated: !this.state.updated});
            EditVenueHandle(this.props)
        }
    }
    render(){

        return(

            <div className="row formButtons ">
                <Button buttonName={ this.props.name } buttonStyle={ "btn-primary pull-right" } onClick={this.onAdd}/>
                <Button buttonName={ "CANCEL" } buttonStyle={ "btn-danger pull-right mRight10"} onClick={this.OnCancel} />

                {
                    this.state.cancelled? <ConfirmExit type="Venue" linkTo="/venues" /> : null
                }
                {
                    this.state.added? <Confirm type="Venue" confirmType="Added" linkTo="/venues" /> : null
                }
                {
                    this.state.updated? <Confirm type="Venue" confirmType="Updated" linkTo="/venues" /> : null
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        venue: state.venue,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueReset: () => {
            const action = {type: 'VENUE_RESET'};
            dispatch(action);
        },
        eventReset: () => {
            const action = {type: 'EVENT_RESET'};
            dispatch(action);
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Buttons);