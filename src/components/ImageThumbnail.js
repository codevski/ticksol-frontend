import React from 'react';
import VenueImage from '../images/arena.jpg';

const ImageThumbnail = props => (
    <div>

        {/*<div className="col-sm-6 col-md-4">*/}
            {/*<div className="thumbnail">{props.name}*/}
                {/*<i className="fa fa-cog pull-right" aria-hidden="true" />*/}
                    <img src={props.src} alt="l" className="Venue image-thumb"/>
            {/*</div>*/}
        {/*</div>*/}
    </div>
);

export default ImageThumbnail;