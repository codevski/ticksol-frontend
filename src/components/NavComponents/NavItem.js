import React from 'react';
import { NavLink } from 'react-router-dom';

const NavItem = (props) => (

    <NavLink exact className="btn MenuBtn" to={props.to}>
        <div className="menuIcons">
            <i className={ props.style } aria-hidden="true" />
        </div>
        <div className="menuName">
            { props.name }
        </div>
    </NavLink>
);

export default NavItem;

