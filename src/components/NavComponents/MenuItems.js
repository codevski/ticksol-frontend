import React from 'react';
import NavItem from './NavItem'

const MenuItems = (props) => (

    <div className="MenuItems">
        <NavItem style="fa fa-tachometer fontAwesome" name="Dashboard" to="/" />
        <NavItem style="fa fa-map-marker fontAwesome" name="Venues" to="/venues" />
        <NavItem style="fa fa-calendar-o fontAwesome" name="Events" to="/events" />
        <NavItem style="fa fa-puzzle-piece fontAwesome" name="Add-ons" to="/addons" />
        {(() => {
            if(props.addonData != null) {
                // return <NavItem style="fa fa-puzzle-piece fontAwesome" name={props.addonData.type} to='/addons/reporting' />
                console.log(props.addonData)
                return props.addonData.map((name, i) => {
                    console.log(name);

                    return <NavItem key={i} style="fa fa-calendar-o fontAwesome" name={name} to='/addons/calendar'/>;

                })
            }
            // switch (props.addonData) {
            //     case "Ticket Sales Reporting":   return <NavItem style="fa fa-puzzle-piece fontAwesome" name={props.addonData} to='/addons/calender' />
            //     case "Reporting":   return <NavItem style="fa fa-puzzle-piece fontAwesome" name={props.addonData} to='/addons/reporting' />
            //     default:          return null;
            // }
        })()}
        <NavItem style="fa fa-cogs fontAwesome" name="Settings" to="/settings" />

    </div>
);

export default MenuItems;