import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import { connect } from 'react-redux';

class ConfirmAdd extends React.Component{
    constructor(){
        super();
        this.state= {
            show: true
        };
        this.onAdd = this.onAdd.bind(this);
    };

    onAdd(){
        const { venueReset } = this.props;
        const { eventReset } = this.props;

        if(this.props.type === "Venue"){
            venueReset();
            this.setState({ show: false});
        }else if(this.props.type === "Settings"){
            window.location.reload();
        } else{
            eventReset();
            this.setState({ show: false })
        }
    };
    render(){
        let close = () => this.setState({ show: false});

        return(
            <div>
                <Modal show={this.state.show} onHide={close} container={this}>
                    <Modal.Header closeButton>
                        <Modal.Title>Confirmation</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <h4>{this.props.type} {this.props.confirmType}. Thank you.</h4>

                    </Modal.Body>

                    <Modal.Footer>
                        <Link to={this.props.linkTo}>
                            <Button onClick={this.onAdd}>OK</Button>
                        </Link>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        venueItems: state.venue,
        eventItems: state.event,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueReset: () => {
            const action = {type: 'VENUE_RESET'};
            dispatch(action);
        },
        eventReset: () => {
            const action = {type: 'EVENT_RESET'};
            dispatch(action);
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmAdd);