import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal, Popover, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { connect } from 'react-redux';

class ConfirmExit extends React.Component{
    constructor(){
        super();
        this.state= {
            show: true
        };
        this.onCancel = this.onCancel.bind(this);
    };

    onCancel(){
        const { eventReset } = this.props;
        const { venueReset } = this.props;

        if(this.props.type === 'Venue'){
            venueReset();
            this.setState({ show: false});
        }else{
            eventReset();
            this.setState({ show: false});
        }
    };
   render(){
       let close = () => this.setState({ show: false});
       return(
            <div>
                <Modal show={this.state.show} onHide={close} container={this}>
                    <Modal.Header closeButton>
                        <Modal.Title>Cancellation Confirmation</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <h4>Are you sure?</h4>
                        <p>If your press 'Yes' you will lose any unsaved changes.</p>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={close}>Cancel</Button>
                        <Link to={this.props.linkTo}>
                        <Button onClick={this.onCancel}>Yes</Button>
                        </Link>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        venueItems: state.venue,
        eventItems: state.event,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueReset: () => {
            const action = {type: 'VENUE_RESET'};
            dispatch(action);
        },
        eventReset: () => {
            const action = {type: 'EVENT_RESET'};
            dispatch(action);
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmExit);