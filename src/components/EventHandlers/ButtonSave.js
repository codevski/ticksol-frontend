import React from 'react';
import Button from '../../components/FormComponents/Button.js';
import { SaveVenueHandle, UpdateSettings, UpdateAccount } from '../../components/EventHandlers/TickSolAPI';
import Confirm from '../../components/EventHandlers/Confirm';
import { connect } from 'react-redux';

export class ButtonSave extends React.Component{
    constructor(){
        super();
        this.state= {
            saved: false,
            saveSettings: false,
            saveAccount: false,
        };
        this.onSave = this.onSave.bind(this);
    };

    onSave(){
        if(this.props.type === 'Save'){
            this.setState({saved: !this.state.saved});
            SaveVenueHandle(this.props)
        }else if(this.props.type === "Settings"){
            this.setState({saveSettings: !this.state.saveSettings});
            UpdateSettings(this.props);
        }else if(this.props.type === "Account"){
            this.setState({saveAccount: !this.state.saveAccount});
            UpdateAccount(this.props);
        }

        };

    render(){

        return(
            <div>
                <Button buttonName={ this.props.name } buttonStyle={ "btn-default pull-left" } onClick={this.onSave}/>

                {
                    this.state.saved? <Confirm type="Venue" confirmType="Saved" linkTo="/venues" /> : null
                }
                {
                    this.state.saveSettings? <Confirm type="Settings" confirmType="Updated" linkTo="/settings" /> : null
                }
                {
                    this.state.saveAccount? <Confirm type="Account" confirmType="Updated" linkTo="/settings/account" /> : null
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        venue: state.venue,
        settings: state.settings,
        user: state.user,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueReset: () => {
            const action = {type: 'VENUE_RESET'};
            dispatch(action);
        },
        eventReset: () => {
            const action = {type: 'EVENT_RESET'};
            dispatch(action);
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ButtonSave);