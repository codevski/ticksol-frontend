import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import Confirm from '../../components/EventHandlers/Confirm';

export const AddEventHandle = (props) => {

    const { event } = props;
    const { eventReset }  = props;
    const url = 'http://128.199.124.144/api/events';
    const day = new Date(event.eventDate).getDate();
    const month = new Date(event.eventDate).getMonth() + 1;
    const year = new Date(event.eventDate).getFullYear();
    const eventDate = year+"-"+month+"-"+day;
    /*const fd = new FormData();
    fd.append("name", event.eventName);
    fd.append( "venue_id", "4");
    fd.append("conduct_date", "2017-10-31");
    fd.append("start_time", "12:00:01");
    fd.append("end_time", "13:00:01");
    console.log(fd);*/
    console.log("1  -"+event.eventVenue);
    const data = {
        "name": event.eventName,
        "image_url": event.eventImgURL,
        "venue_id": event.eventVenue,
        "conduct_date": eventDate,
        "start_time": "12:00:01",
        "end_time": "13:00:01",
        "description": event.eventDescription,
        "seating_category": event.eventSeatingCat,
        "price": event.eventPrice,
    };
    console.log("data: "+data);
    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.post(url, data)
        .then( (res ) => {
            eventReset();
        })
        .catch(function (err) {
            console.error('err', err);
        });
};

export const EditEventHandle = (props) => {

    const { event } = props;
    const { eventReset }  = props;
    const url = 'http://128.199.124.144/api/events/' + props.postID;

    const day = new Date(event.eventDate).getDate();
    const month = new Date(event.eventDate).getMonth() + 1;
    const year = new Date(event.eventDate).getFullYear();
    const eventDate = year+"-"+month+"-"+day;
    console.log(event.eventName);
    console.log(event.eventVenue);
    const data = {
        "name": event.eventName,
        "image_url": event.eventImgURL,
        "venue_id": event.eventVenue,
        "conduct_date": eventDate,
        "start_time": "12:00:01",
        "end_time": "13:00:01",
        "description": event.eventDescription,
        "seating_category": event.eventSeatingCat,
        "price": event.eventPrice,
    };
    console.log("data: "+data);
    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.put(url, data)
        .then( (res ) => {
            // File uploaded successfully
            eventReset();

        })
        .catch(function (err) {
            console.error('err', err);
        });

};

export const DeleteEventHandle= () => {

};

export const GetEvent= () => {

};

const mapStateToProps = (state) => {
    return {
        event: state.event,

    }
};

export default connect(mapStateToProps)(AddEventHandle,EditEventHandle,DeleteEventHandle,GetEvent);
