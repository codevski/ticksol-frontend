import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

export const AddAddonHandle = (props) => {
    console.log(props.id);

    const url = 'http://128.199.124.144/api/plugins/'+props.id;
    console.log(url);
    console.log(props.is_enabled);
    const data = {
        "is_enabled": props.is_enabled,
        "name": props.type,
    };


    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.put(url, data)
        .then( (res ) => {
            console.log("updated")
            window.location.href = "/addons"
            // File uploaded successfully
        })
        .catch(function (err) {
            console.error('err', err);
        });
};

const mapStateToProps = (state) => {
    return {
        addon: state.addon,

    }
};

export default connect(mapStateToProps)(AddAddonHandle);