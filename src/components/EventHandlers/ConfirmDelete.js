import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import axios from 'axios';

class ConfirmDelete extends React.Component{
    constructor(){
        super();
        this.state= {
            show: true
        };
        this.onDelete = this.onDelete.bind(this);
    };

    onDelete(id){
        const { venueReset } = this.props;
        const { eventReset } = this.props;
        const type = this.props.type;
        console.log(id);
        axios.delete('http://128.199.124.144/api/'+type+'/'+ id)
            .then((res) => {
                venueReset();
                eventReset();
                this.setState({ show: false});
                this.props.deletedStatus();
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };

    render(){
        let close = () => {
            this.props.deletedStatus(false);
            this.setState({ show: false});
        }
        return(
            <div>
                {console.log(this.props.postID)}
                <Modal show={this.state.show} onHide={close} container={this}>
                    <Modal.Header closeButton>
                        <Modal.Title>Deletion Confirmation</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <h4>Are you sure?</h4>
                        <p>If your press 'Yes', {this.props.name} will be deleted.</p>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={close}>Cancel</Button>
                       <Link to={this.props.linkTo}>
                            <Button onClick={this.onDelete.bind(this, this.props.postID)}>Yes</Button>
                       </Link>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        venueItems: state.venue,
        eventItems: state.event,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueReset: () => {
            const action = {type: 'VENUE_RESET'};
            dispatch(action);
        },
        eventReset: () => {
            const action = {type: 'EVENT_RESET'};
            dispatch(action);
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ConfirmDelete);

