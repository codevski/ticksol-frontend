import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';

export const AddVenueHandle = (props) => {

    const { venue } = props;
    const { venueReset }  = props;
    const url = 'http://128.199.124.144/api/venues/view';

    const fd = new FormData();
    fd.append("name", venue.venueName);
    fd.append("address", venue.venueAddressOne);
    fd.append("capacity", venue.venueCapacity);

    const data = {
        "name": venue.venueName,
        "address_line_1": venue.venueAddressOne,
        "address_line_1": venue.venueAddressTwo,
        "city": venue.venueCity,
        "state": venue.venueState,
        "postcode": venue.venuePostcode,
        "country": venue.venueCountry,
        "contact_num": venue.venueContactNumber,
        "email": venue.venueEmail,
        "capacity": venue.venueCapacity,
        "website_url": venue.url,
        "wheelchair_access": venue.venueWheelchair,
        "parking": venue.venueParking,
        "trains": venue.venueTrains,
        "trams": venue.venueTrams,
        "busses": venue.venueBusses,
        "taxi_uber": venue.venueTaxiUber,
        "restaurants": venue.venueRestaurants,
        "pubs": venue.venuePubs,
        "pending": false,
    };

    console.log(fd);
    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.post(url, data)
        .then( (res ) => {
            // File uploaded successfully
           venueReset();

        })
        .catch(function (err) {
            console.error('err', err);
        });
};

export const SaveVenueHandle = (props) => {
    console.log('im here');
    const { venue } = props;
    const { venueReset }  = props;
    const url = 'http://128.199.124.144/api/venues/view';

    const fd = new FormData();
    fd.append("name", venue.venueName);
    fd.append("address", venue.venueAddressOne);
    fd.append("capacity", venue.venueCapacity);

    const data = {
        "name": venue.venueName,
        "address_line_1": venue.venueAddressOne,
        "address_line_1": venue.venueAddressTwo,
        "city": venue.venueCity,
        "state": venue.venueState,
        "postcode": venue.venuePostcode,
        "country": venue.venueCountry,
        "contact_num": venue.venueContactNumber,
        "email": venue.venueEmail,
        "capacity": venue.venueCapacity,
        "website_url": venue.url,
        "wheelchair_access": venue.venueWheelchair,
        "parking": venue.venueParking,
        "trains": venue.venueTrains,
        "trams": venue.venueTrams,
        "busses": venue.venueBusses,
        "taxi_uber": venue.venueTaxiUber,
        "restaurants": venue.venueRestaurants,
        "pubs": venue.venuePubs,
        "pending": venue.venuePending,
    };

    console.log(fd);
    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.post(url, data)
        .then( (res ) => {
            // File uploaded successfully
            venueReset();

        })
        .catch(function (err) {
            console.error('err', err);
        });
};

export const EditVenueHandle = (props) => {

    const { venue } = props;
    const { venueReset }  = props;
    const url = 'http://128.199.124.144/api/venues/view/' + props.postID;
    console.log(props.postID);
    const fd = new FormData();
    fd.append("name", venue.venueName);
    fd.append("address", venue.venueAddressOne);
    fd.append("capacity", venue.venueCapacity);

    const data = {
        "name": venue.venueName,
        "address_line_1": venue.venueAddressOne,
        "address_line_2": venue.venueAddressTwo,
        "city": venue.venueCity,
        "state": venue.venueState,
        "postcode": venue.venuePostcode,
        "country": venue.venueCountry,
        "contact_num": venue.venueContactNumber,
        "email": venue.venueEmail,
        "capacity": venue.venueCapacity,
        "website_url": venue.url,
        "wheelchair_access": venue.venueWheelchair,
        "parking": venue.venueParking,
        "trains": venue.venueTrains,
        "trams": venue.venueTrams,
        "busses": venue.venueBusses,
        "taxi_uber": venue.venueTaxiUber,
        "restaurants": venue.venueRestaurants,
        "pubs": venue.venuePubs,
        "pending": false,
    };

    console.log(fd);
    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.put(url, data)
        .then( (res ) => {
            // File uploaded successfully
            venueReset();

        })
        .catch(function (err) {
            console.error('err', err);
        });

};

export const UpdateSettings = (props) => {

    const { settings } = props;
    const url = 'http://128.199.124.144/api/settings/1';

    const data = {
        "company_name": settings.companyName.toUpperCase(),
    };

    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.put(url, data)
        .then( (res ) => {

            // File uploaded successfully
        })
        .catch(function (err) {
            console.error('err', err);
        });

};

export const UpdateAccount = (props) =>{
    const { user } = props;

    const url = 'http://128.199.124.144/api/users/' + user.userID;

    const data = {
        "name": user.userName,
        "password": user.userPassword,
        "email": user.userEmail,
        "image_url": user.userImage,
    };

    const config = {
        headers: { "X-Requested-With": "XMLHttpRequest" },
        onUploadProgress: function(progressEvent) {
            // Do something with the native progress event
        }
    };

    axios.put(url, data)
        .then( (res ) => {
            window.location.reload();
            // File uploaded successfully
        })
        .catch(function (err) {
            console.error('err', err);
        });

};

const mapStateToProps = (state) => { console.log(state);
    return {
        venue: state.venue,
        settings: state.settings,

    }
};

export default connect(mapStateToProps)(AddVenueHandle, SaveVenueHandle, EditVenueHandle,UpdateSettings, UpdateAccount);
