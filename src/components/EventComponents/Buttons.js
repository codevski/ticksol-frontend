import React from 'react';
import Button from '../../components/FormComponents/Button.js';
import ConfirmExit from "../../components/EventHandlers/Cancel";
import Confirm from '../../components/EventHandlers/Confirm';
import { connect } from 'react-redux';
import { AddEventHandle, EditEventHandle } from '../../components/EventHandlers/EventTickSolAPI';

export class Buttons extends React.Component{
    constructor(){
        super();
        this.state= {
            cancelled: false,
            added: false,
            updated: false,
        };
        this.OnCancel = this.OnCancel.bind(this);
        this.onAdd = this.onAdd.bind(this);
    };

    OnCancel(){
        this.setState({cancelled: !this.state.cancelled});
    }

    onAdd(){
        if(this.props.add === 'add') {
            this.setState({added: !this.state.added});
            AddEventHandle(this.props)
            /*AddEventHandle(this.props)*/
        }else{
            this.setState({updated: !this.state.updated});
            EditEventHandle(this.props)
            /*EditEventHandle(this.props)*/
        }
    }
    render(){

        return(

            <div className="row formButtons ">
                <Button buttonName={ this.props.name } buttonStyle={ "btn-primary pull-right" } onClick={this.onAdd}/>
                <Button buttonName={ "CANCEL" } buttonStyle={ "btn-danger pull-right mRight10"} onClick={this.OnCancel} />

                {
                    this.state.cancelled? <ConfirmExit type="Event" linkTo="/events" /> : null
                }
                {
                    this.state.added? <Confirm type="Event" confirmType="Added" linkTo="/events" /> : null
                }
                {
                    this.state.updated? <Confirm type="Event" confirmType="Updated" linkTo="/events" /> : null
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        event: state.event,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        venueReset: () => {
            const action = {type: 'VENUE_RESET'};
            dispatch(action);
        },
        eventReset: () => {
            const action = {type: 'EVENT_RESET'};
            dispatch(action);
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Buttons);