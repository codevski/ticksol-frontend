import React from 'react';
import axios from 'axios';

class TicketsWidget extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            Data: [],
        };
    };
    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/dashboard')
            .then((res) => {
                console.log(res.data.metrics.venue_events)
                console.log("TEST")
                this.setState({Data: res.data.metrics.venue_events});
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };
    render(){
        return(
            <div className="col-lg-4 col-md-6">
                <div className="panel panel-success" style={{borderColor: '#34495e'}}>
                    <div className="panel-heading" style={{backgroundColor: '#34495e',
                        color: 'white',
                        borderColor: '#34495e'}}>
                        <div className="row">
                            <div className="col-xs-5">
                                Venue Stats
                            </div>
                            <div className="col-xs-9 text-right">

                            </div>
                        </div>
                    </div>

                    <div className="panel-footer">
                        <table>
                            <tr>
                                <th>ID</th>
                                <th>Venue</th>
                                <th>Events</th>
                            </tr>
                            {this.state.Data.map((venData, i) => {
                                return (
                                    <tr>
                                        <td>{venData.venue_id}</td>
                                        <td>{venData.venue_name}</td>
                                        <td>{venData.events}</td>
                                    </tr>
                                );
                            })}

                        </table>

                        <div className="clearfix"></div>
                    </div>

                </div>
            </div>
        );
    }
}

export default TicketsWidget;