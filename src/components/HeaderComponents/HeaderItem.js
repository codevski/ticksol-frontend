import React from 'react';
import { NavLink } from 'react-router-dom';

const HeaderItem = (props) => (
    <NavLink className="btn MenuBtn eventsBtn" to={ props.to } activeClassName="active">
        <div className="menuName">
            { props.name }
        </div>
    </NavLink>
);

export default HeaderItem;


