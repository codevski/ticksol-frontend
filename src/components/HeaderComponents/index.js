import React from 'react';
import HeaderItem from './HeaderItem';

const HeaderTemplate = (props) => (
    <div>
        <div className="row">
            <div className="col-lg-12 colBottomBorder">

                { props.data.map( (objectItem, i) => {
                    return(
                        <HeaderItem key={ i } name={ objectItem.name } to={ objectItem.link } />
                    );
                })}
            </div>
        </div>
        <div className="bottomBorder" />
    </div>
    );

export default HeaderTemplate;


