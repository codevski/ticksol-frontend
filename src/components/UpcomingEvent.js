import React from 'react';
import axios from 'axios';


class UpcomingEvents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Data: [],
        };
    };

    componentDidMount() {
        // axios.get('http://128.199.124.144/ticksol/public/api/v1/venues')
        axios.get('http://128.199.124.144/api/dashboard/events')
            .then((res) => {
                console.log(res.data.events)
                this.setState({Data: res.data.events});
            })
            .catch(function (err) {
                console.error('err', err);
            });
    };

    render() {
        return (
            <div className="col-lg-8 col-md-6">
                <div className="panel panel-success" style={{borderColor: '#34495e'}}>
                    <div className="panel-heading" style={{
                        backgroundColor: '#34495e',
                        color: 'white',
                        borderColor: '#34495e'
                    }}>
                        <div className="row">
                            <div className="col-xs-5">
                                Upcoming Events
                            </div>
                            <div className="col-xs-9 text-right">

                            </div>
                        </div>
                    </div>


                    <div className="panel-footer">
                        {console.log(this.state.Data)}

                        {this.state.Data.map((venData, i) => {
                            return (
                                <div className="col-md-6">
                                    <div className="thumbnail eventThumb"><b>{venData.name}</b>
                                        <img src={venData.image_url} alt="l" className="upcoming-event"/>
                                        <p>
                                            <b>Date: </b>{venData.start_date}<br />
                                            <b>Status:</b><span className="current"> Current</span>
                                        </p>
                                    </div>
                                </div>

                            );
                        })}


                        <div className="clearfix"></div>
                    </div>

                </div>
            </div>
        );
    }
}
export default UpcomingEvents;