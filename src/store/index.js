import { createStore } from 'redux';
import reducers from './reducers/index';

const persistedState = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {};

const store = createStore(reducers, persistedState);

store.subscribe(()=>{
    localStorage.setItem('user', JSON.stringify(store.getState()))
});

export default store;

