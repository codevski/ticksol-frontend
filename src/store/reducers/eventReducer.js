
const initialState = {
    eventName: '',
    eventVenue: '',
    eventStart: '',
    eventFinish: '',
    eventSeatingCat: '',
    eventPrice: '',
    eventDescription: '',
    eventDate: '',
    eventImgURL: '',
};

const eventReducer = (state = initialState, action) => {

    switch (action.type){

        case 'EVENT_NAME':
            return Object.assign({}, state, { eventName: action.text});

        case 'EVENT_VENUE':
            return Object.assign({}, state, { eventVenue: action.text});

        case 'EVENT_START':
            return Object.assign({}, state, { eventStart: action.text});

        case 'EVENT_FINISH':
            return Object.assign({}, state, { eventFinish: action.text});

        case 'EVENT_SEATING':
            return Object.assign({}, state, { eventSeatingCat: action.text});

        case 'EVENT_PRICE':
            return Object.assign({}, state, { eventPrice: action.text});

        case 'EVENT_DESCRIPTION':
            return Object.assign({}, state, {eventDescription: action.text});

        case 'EVENT_DATE':
            return Object.assign({}, state, {eventDate: action.text});

        case 'EVENT_IMG':
            return Object.assign({}, state, {eventImgURL: action.text});

        case 'EVENT_RESET':
            state = initialState;

        default:
            return state;
    }
};

export default eventReducer;