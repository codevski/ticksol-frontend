const initialState = {
    addonSwitch: false,
};

const addonnReducer = (state = initialState, action) => {

    switch (action.type){

        case 'ADDON_SWITCH':
            return Object.assign({}, state, { addonSwitch: action.checked});

        case 'ADDON_RESET':
            state = initialState;

        default:
            return state;
    }
};

export default addonnReducer;