import { combineReducers } from 'redux';
import venueReducer from './venueReducer';
import eventReducer from './eventReducer';
import addonReducer from './AddOnReducer';
import settingsReducer from './SettingsReducer';
import UserReducer from './UserReducer';

const reducers = combineReducers(
        {
            venue: venueReducer,
            event: eventReducer,
            addon: addonReducer,
            settings: settingsReducer,
            user: UserReducer,
        }
    );

export default reducers;