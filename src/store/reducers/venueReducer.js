
const initialState = {
    inputValue: ' ',
    someValue: ' ',
    venueName: ' ',
    venueAddressOne: ' ',
    venueAddressTwo: ' ',
    venueCity: ' ',
    venueState: ' ',
    venuePostcode: ' ',
    venueCountry: ' ',
    venueContactNumber: ' ',
    venueEmail: ' ',
    venueCapacity: ' ',
    venueWheelchair: false,
    venueParking: false,
    venueTrains: false,
    venueTrams: false,
    venueBusses: false,
    venueTaxiUber: false,
    venueRestaurants: false,
    venuePubs: false,
    venueDescription: '',
    url: ' ',
    venuePending: true,
};

const venueReducer = (state = initialState, action) => {

    switch (action.type){

        case 'INPUT_CHANGE':
            return Object.assign({}, state, { inputValue: action.text });

        case 'NEW_INPUT_CHANGE':
            return Object.assign({}, state, { someValue: action.text });

        case 'VENUE_NAME':
            return Object.assign({}, state, { venueName: action.text});

        case 'VENUE_ADDRESS_ONE':
            return Object.assign({}, state, { venueAddressOne: action.text});

        case 'VENUE_ADDRESS_TWO':
            return Object.assign({}, state, { venueAddressTwo: action.text});

        case 'VENUE_CITY':
            return Object.assign({}, state, { venueCity: action.text});

        case 'VENUE_STATE':
            return Object.assign({}, state, { venueState: action.text});

        case 'VENUE_POSTCODE':
            return Object.assign({}, state, { venuePostcode: action.text});

        case 'VENUE_COUNTRY':
            return Object.assign({}, state, { venueCountry: action.text});

        case 'VENUE_CONTACT_NO':
            return Object.assign({}, state, { venueContactNumber: action.text});

        case 'VENUE_EMAIL':
            return Object.assign({}, state, { venueEmail: action.text});

        case 'VENUE_CAPACITY':
            return Object.assign({}, state, { venueCapacity: action.number});

        case 'VENUE_WHEELCHAIR':
            return Object.assign({}, state, { venueWheelchair: action.checked});

        case 'VENUE_PARKING':
            return Object.assign({}, state, { venueParking: action.checked});

        case 'VENUE_TRAINS':
            return Object.assign({}, state, { venueTrains: action.checked});

        case 'VENUE_TRAMS':
            return Object.assign({}, state, { venueTrams: action.checked});

        case 'VENUE_BUSSES':
            return Object.assign({}, state, { venueBusses: action.checked});

        case 'VENUE_TAXI_UBER':
            return Object.assign({}, state, { venueTaxiUber: action.checked});

        case 'VENUE_RESTAURANT':
            return Object.assign({}, state, { venueRestaurants: action.checked});

        case 'VENUE_PUBS':
            return Object.assign({}, state, { venuePubs: action.checked});

        case 'URL':
            return Object.assign({}, state, { url: action.text});

        case 'VENUE_DESCRIPTION':
            return Object.assign({}, state, { venueDescription: action.text});

        case 'VENUE_PENDING':
            return Object.assign({}, state, { venuePending: action.text});

        case 'VENUE_RESET':
            state = initialState;

        default:
            return state;
    }
};

export default venueReducer;