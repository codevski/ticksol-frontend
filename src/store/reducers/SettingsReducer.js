const initialState = {
    companyName: '',
};

const settingsReducer = (state = initialState, action) => {

    switch (action.type){

        case 'SETTINGS_COMP_NAME':
            return Object.assign({}, state, { companyName: action.text});

        case 'SETTINGS_RESET':
            state = initialState;

        default:
            return state;
    }
};

export default settingsReducer;