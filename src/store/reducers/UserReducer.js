const initialState = {
    userLoggedIn: '',
    userName: '',
    userPassword: '',
    userEmail: '',
    userImage: '',
    userID: '',
};

const UserReducer = (state = initialState, action) => {
console.log(action);
    switch (action.type){

        case  'USER_LOGGED_IN':
            return Object.assign({}, state, { userLoggedIn: action.text});

        case 'USER_NAME':
            return Object.assign({}, state, { userName: action.text});

        case 'USER_PASSWORD':
            return Object.assign({}, state, { userPassword: action.text});

        case 'USER_EMAIL':
            return Object.assign({}, state, { userEmail: action.text});

        case 'USER_IMAGE':
            return Object.assign({}, state, { userImage: action.text});

        case 'USER_ID':
            return Object.assign({}, state, { userID: action.text});

        case 'USER_RESET':
            state = initialState;

        default:
            return state;
    }
};

export default UserReducer;