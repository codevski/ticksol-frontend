export const actionCreators = {
    venueNameChanged: (evt) => {
        return { type: 'VENUE_NAME', text: evt.target.value };
    },
    venueUpdateName: evt => {
        return { type: 'VENUE_NAME', text: evt };
    },
    venueAddressOneChanged: (evt) => {
        return {type: 'VENUE_ADDRESS_ONE', text: evt.target.value};
    },
    venueUpdateAddressOne: (evt) => {
        return {type: 'VENUE_ADDRESS_ONE', text: evt};
    },
    venueCapacityUpdate: (evt) => {
        return {type: 'VENUE_CAPACITY', number: evt};
    },
    venueCapacityChange: (evt) => {
        return {type: 'VENUE_CAPACITY', number: evt.target.value};
    },
    venueReset: () => {
        return {type: 'VENUE_RESET'};
    },
    onChangeChecked: (action, evt) => {
        return { type: action, checked: evt.target.checked };
    },
    onChangeInt: (action, evt) => {
        return { type: action, number: evt.target.value };
    },
    onChangeString: (action, evt) => {
        return { type: action, text: evt.target.value };
    },

    onUpdateChecked: (action, evt) => {
        return { type: action, checked: evt };
    },
    onUpdateInt: (action, evt) => {
        return { type: action, number: evt };
    },
    onUpdateString: (action, evt) => {
        return { type: action, text: evt };
    }
};