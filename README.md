This project was created with [React](https://facebook.github.io/react/).

Below you will find some information on how to perform common tasks.<br>

## Table of Contents

- [How to install](#install)
- [Sending Feedback](#sending-feedback)

## Install

Create React App is divided into two packages:

* `npm install` to install.
* `npm run` is to run development .

More to come.

## Sending Feedback

We are always open to [your feedback](https://github.com/johnnylak/ticksol-frontend/issues).